=begin
= NAME

update-cmigemo-dict - create/update cmigemo dictionaries for Debian

= DESCRIPTION

update-cmigemo-dict is simple shell script in order to create cmigemo
dictionary from SKK-JISYO file. The default behavior is create CP932,
EUC-JP, UTF-8 dictionaries from SKK-JISYO.L.

= USAGE

 update-cmigemo-dict [FILE]

= OPTIONS:

: FILE
  If you want to create custom cmigemo dictionaries, set full path of
  SKK-JISYO file

= AUTHOR

This manual page was written by Youhei SASAKI <uwabami@gfd-dennou.org>,
for the Debian GNU/Linux system (but may be used by others).

=end
