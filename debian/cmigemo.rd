=begin
= NAME

cmigemo - C/Migemo Library 1.3 Driver

= USAGE

 cmigemo [OPTIONS]

= OPTIONS:

:-d, --dict <dict>:
  Use a file <dict> for dictionary.
:-s, --subdict <dict>:
  Sub dictionary files. (MAX 8 times)
:-q, --quiet:
  Show no message except results.
:-v, --vim:
  Use vim style regexp.
:-e, --emacs:
  Use emacs style regexp.
:-n, --nonewline:
  Don't use newline match.
:-w, --word <word>:
  Expand a <word> and soon exit.
:-h, --help:
  Show this message.

= AUTHOR

This manual page was written by Youhei SASAKI <uwabami@gfd-dennou.org>,
for the Debian GNU/Linux system (but may be used by others).

=end
